angular.module 'vivienda'
  .config ($logProvider, $localeProvider, toastrConfig) ->
    'ngInject'
    # Enable log
    $logProvider.debugEnabled true
    # Set options third-party lib
    toastrConfig.allowHtml = true
    toastrConfig.timeOut = 5000
    toastrConfig.positionClass = 'toast-top-right'
    toastrConfig.progressBar = true
    $localeProvider.id = 'es-bo'
    