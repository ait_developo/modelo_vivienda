describe 'controllers', () ->
  vm = undefined

  beforeEach module 'vivienda'

  beforeEach inject ($controller, webDevTec, toastr) ->
    spyOn(webDevTec, 'getTec').and.returnValue [{}, {}, {}, {}, {}]
    spyOn(toastr, 'info').and.callThrough()
    vm = $controller 'MainController'

  it 'should have a timestamp creation date', () ->
    expect(creationDate).toEqual jasmine.any Number

  it 'should define animate class after delaying timeout ', inject ($timeout) ->
    $timeout.flush()
    expect(classAnimation).toEqual 'rubberBand'

  it 'should show a Toastr info and stop animation when invoke showToastr()', inject (toastr) ->
    showToastr()
    expect(toastr.info).toHaveBeenCalled()
    expect(classAnimation).toEqual ''

  it 'should define more than 5 awesome things', () ->
    expect(angular.isArray(awesomeThings)).toBeTruthy()
    expect(awesomeThings.length == 5).toBeTruthy()
