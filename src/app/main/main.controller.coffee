
angular.module 'vivienda'
  .controller 'MainController', ($scope, $timeout, RegionService, ConstanteService, ReportService, toastr, $translate, WizardHandler, $window, $filter) ->
    'ngInject'
    vm = @

    $scope.numberInputSettings =
      width: '100%'
      height: '30px'
      decimalSeparator: ','
      groupSeparator: '.'

    $scope.currentDate = new Date().toString()
    $scope.departamentos = RegionService.getDepartamentos()
    $scope.posAreaExtensiva = 0

    vm.estadoInformacion = true
    vm.estadoHogares = true
    vm.estadoAreaExtensiva = true
    vm.estadoAreaExtensivaFinal = true
    vm.imprimir = true
    vm.viviendasDisponibles = true

    # Total de areas extensivas que se llenaron
    $scope.areasExtensivas = []

    # Calculo general
    $scope.calculo = {
      cantidadAreasExtensivas: 1
    }

    ###*
     * Se encarga de volver a un paso anterior
     * @return undefined
    ###
    $scope.volverAtras = ->
      if !vm.estadoInformacion and vm.estadoHogares and vm.estadoAreaExtensiva and vm.estadoAreaExtensivaFinal
        vm.estadoInformacion = true
        WizardHandler.wizard().previous()
      if !vm.estadoInformacion and !vm.estadoHogares and vm.estadoAreaExtensiva and vm.estadoAreaExtensivaFinal
        if $scope.posAreaExtensiva == 0
          vm.estadoHogares = true
          WizardHandler.wizard().previous()
        else
          $scope.posAreaExtensiva -= 1
          $scope.areaExtensiva = $scope.areasExtensivas[$scope.posAreaExtensiva]

      if !vm.estadoInformacion and !vm.estadoHogares and !vm.estadoAreaExtensiva and vm.estadoAreaExtensivaFinal
        console.log $scope.areasExtensivas
        console.log $scope.posAreaExtensiva
        $scope.areasExtensivas = $scope.calculo.areasExtensivas
        $scope.areaExtensiva = $scope.areasExtensivas[$scope.posAreaExtensiva]
        vm.estadoAreaExtensiva = true
        WizardHandler.wizard().previous()
      # if !vm.estadoInformacion and !vm.estadoHogares and vm.estadoAreaExtensiva and vm.estadoAreaExtensivaFinal
      #   vm.estadoHogares = true
      #   WizardHandler.wizard().previous()
      # console.log vm.estadoInformacion
      # console.log vm.estadoHogares
      # console.log vm.estadoAreaExtensiva
      # console.log vm.estadoAreaExtensivaFinal
      # console.log vm.imprimir

    # Varible auxiliar para almacenar un area extensiva
    $scope.areaExtensiva = {
      numeroPisos: 1
    }

    mostrarError = () ->
      $translate('VALIDAR_CAMPOS').then (errorText) ->
        toastr.warning errorText, 'FORMULARIO'

    $scope.sliderOptions =
      options:
        minValue: 1
        floor: 1
        ceil: 30
        step: 1
        showTicks: true
        showSelectionBar: true

    $scope.aresOptions =
      inValue: 1
      floor: 1
      ceil: 5
      step: 1
      showTicks: true
      showSelectionBar: true


    # Validacion de la informacion
    $scope.validacionInformacion = () ->
      if $scope.calculo.departamento? and $scope.calculo.municipio? and $scope.calculo.areaUrbana? and $scope.calculo.profesional?
        vm.estadoInformacion = false
        return true
      mostrarError()
      return false

    # Validacion del paso uno
    $scope.validacionPasoUno = () ->
      if $scope.calculo.poblacionActualAreaIntensiva? and $scope.calculo.poblacionAreaIntensiva? and $scope.calculo.miembrosFamilia? and $scope.calculo.viviendasConstruir?
        vm.estadoHogares = false
        return true
      mostrarError()
      return false

    # Validacion del paso dos
    $scope.validacionPasoDos = () ->
      $scope.agregarAreaExtensiva()
      validacion = $scope.controlarCantidadViviendas()

      if !validacion.estado
        return validacion.estado

      $scope.calculo.areasExtensivas = $scope.areasExtensivas
      $scope.areasExtensivas = []
      $scope.calcularPromedios()
      vm.estadoAreaExtensiva = false if validacion.estado
      return validacion.estado

    # Validacion del paso tres
    $scope.validacionPasoTres = () ->
      if $scope.calculo.superficieFuturaAreaUrbana? and $scope.calculo.totalViviendaSocial?
        vm.estadoAreaExtensivaFinal = false
        return true
      mostrarError()
      return false

    # Para evitar
    $scope.validacionAreasExtensivas = () ->
      if $scope.calculo.areasExtensivas?
        return false
      return true

    # Para validar los ingresos
    $scope.validacionesIngreso =
      informacion: () ->
        return vm.estadoInformacion
      hogares: () ->
        return vm.estadoHogares
      areaExtensiva: () ->
        return vm.estadoAreaExtensiva
      areaExtensivaTotal: () ->
        return vm.estadoAreaExtensivaFinal
      imprimir: () ->
        return vm.estadoInformacion

    # Calculo que el area extensiva requerira en cinco anios
    $scope.$watchCollection('[calculo.poblacionAreaIntensiva, calculo.miembrosFamilia]', (newValues) ->
      if newValues[1] != 0
        $scope.calculo.cincoAnios = ConstanteService.redondear(newValues[0] / newValues[1], 0)
    )

    # Calculo del area extensiva se construiran
    $scope.$watchCollection('[calculo.cincoAnios, calculo.viviendasConstruir]', (newValues) ->
      if newValues[1] > newValues[0]
        toastr.warning "Error el valor de #{newValues[1]} no es Valido el maximo permitido es #{newValues[0]}"
        $scope.calculo.viviendasConstruir = newValues[0]
      $scope.calculo.areaExtensivaConstruir = ConstanteService.redondear(newValues[0] - newValues[1])
    )

    # calulo del deficit habitacional cuantitaivo en ele area urbana extensiva
    $scope.$watchCollection('[calculo.deficitCuantitativo, calculo.deficitHabitacionalAreaIntensiva]', (newValues) ->
      if newValues[1] < newValues[0]
        $scope.calculo.deficitHabitacionalAreaExtensiva = newValues[0] - newValues[1]
      else
        # newValues[1] = 0
        $scope.calculo.deficitHabitacionalAreaIntensiva = 0
        $scope.calculo.deficitHabitacionalAreaExtensiva = 0

      if newValues[1] == 0
        $scope.calculo.viviendasConstruir = 0
    )

    # calculo total que se requerira en el area urbana intensiva
    $scope.$watchCollection('[calculo.deficitHabitacionalAreaIntensiva, calculo.viviendasConstruir]', (newValues) ->
      $scope.calculo.areaIntensivaConstruirReq = newValues[0] + newValues[1]
    )

    # calculo total que se requerira en el area urbana extensiva
    $scope.$watchCollection('[calculo.deficitHabitacionalAreaExtensiva, calculo.areaExtensivaConstruir]', (newValues) ->
      $scope.calculo.areaExtensivaConstruirReq = newValues[0] + newValues[1]
    )

    # Calculo del predio de Superfice patio
    $scope.$watchCollection('[areaExtensiva.superficieMinimaPredio, areaExtensiva.superificieMinimaVivienda]', (newValues) ->
      if newValues[1] > newValues[0]
        $scope.areaExtensiva.superificieMinimaVivienda = newValues[0]
      $scope.areaExtensiva.predioSuperficiePatio = newValues[0] - newValues[1]
    )

    # Calculo de la densificaciondel area Extensiva
    $scope.$watchCollection('[areaExtensiva.numeroPisos]', (newValues) ->
      if newValues[0] < 1
        toastr.warning "El numero de pisos de ser como minimo 1"
        $scope.areaExtensiva.numeroPisos = 1
      $scope.areaExtensiva.densificacionAreaExtensiva = ConstanteService.getCoheficiente(newValues[0])
    )

    # Calculo  de la superfice total de uso de suelo urbano
    $scope.$watchCollection('[areaExtensiva.numeroViviendas, areaExtensiva.superificieMinimaVivienda, areaExtensiva.predioSuperficiePatio, areaExtensiva.densificacionAreaExtensiva]', (newValues) ->
      $scope.areaExtensiva.superficieTotalUsoSueloUrbano = ((newValues[0] * newValues[1] * newValues[3]) + (newValues[0] * newValues[2] * newValues[3])) / ConstanteService.BASE
    )

    # Calculo del procentaje del otro usos de area extensiva
    $scope.$watchCollection('[areaExtensiva.porcentajeCoeficienteOcupacionUsoPrivado]', (newValues) ->
      if newValues[0] > 1
        toastr.warning "Valor invalido no debe exeder del 100%"
        $scope.areaExtensiva.porcentajeCoeficienteOcupacionUsoPrivado = 0
      $scope.areaExtensiva.porcentajeCoeficienteOcupacionUsoPublico = 1 - newValues[0]
    )

    # Calculo de ocupacio del suelo publico
    $scope.$watchCollection('[areaExtensiva.superficieTotalUsoSueloUrbano, areaExtensiva.superificeTotalAreaExtensiva]', (newValues) ->
      $scope.areaExtensiva.coeficienteOcupacionUsoPublico = newValues[0] + newValues[1]
    )

    # Calculo del coheficiente de ocupacion del suelo privado
    $scope.$watchCollection('[areaExtensiva.porcentajeCoeficienteOcupacionUsoPublico, areaExtensiva.porcentajeCoeficienteOcupacionUsoPrivado, areaExtensiva.coeficienteOcupacionUsoPublico]', (newValues) ->
      $scope.areaExtensiva.coeficienteOcupacionUsoPrivado = newValues[1] * newValues[2] / newValues[0]
    )

    # Calculo de la superficie del area urbana
    $scope.$watchCollection('[areaExtensiva.coeficienteOcupacionUsoPrivado, areaExtensiva.coeficienteOcupacionUsoPublico]', (newValues) ->
      $scope.areaExtensiva.superficieAreaUrbana = newValues[0] + newValues[1]
    )

    $scope.$watchCollection('[calculo.viviendaSocialIntensiva, calculo.viviendaSocialExtensiva]', (newValues) ->
      $scope.calculo.totalViviendaSocial = newValues[0] + newValues[1]
    )

    # Calculo de la futura superficie
    $scope.$watchCollection('[calculo.areaIntensivaConstruirReq, calculo.areaExtensivaConstruirReq]', (newValues) ->
      $scope.calculo.sumaTotalViviendas = newValues[0] + newValues[1]
    )
    $scope.$watchCollection('[calculo.superficieAreaUrbana, calculo.superficeAreaIntensiva, calculo.superficieAreaAgropecuaria, calculo.superficieAreaProteccion]', (newValues) ->
      $scope.calculo.superficieFuturaAreaUrbana = newValues[0] + newValues[1] + newValues[2] + newValues[3]
    )

#    $scope.$watchCollection('[calculo.]')

    $scope.imprimir = () ->
      qrCanvas = $("#qr_code > canvas")[0]
      qrImage = new Image()
      qrImage.src = qrCanvas.toDataURL("image/png", 1)
      $("#codigoQr").append(qrImage).html()
      divContents = $("#div_report").html()
      printWindow = window.open('', '', 'height=400,width=800')
      printWindow.document.write('<html><head><title>Viceministerio de Vivienda y Urbanismo</title>')
      printWindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"  media="all">')
      printWindow.document.write('</head><body>')
      printWindow.document.write(divContents)
      printWindow.document.write('</body>')
      printWindow.document.write('</html>')

      printWindow.document.close()

      $timeout(->
        printWindow.print()
        $window.location.reload()
      , 600)

    # Se ejecuta una vez que se termino tods los pasos
    $scope.finalizar = () ->
      $scope.imprimir()
      # Total de areas extensivas que se llenaron
      $scope.areasExtensivas = []

      # Calculo general
      $scope.calculo = {
      }

      # Varible auxiliar para almacenar un area extensiva
      $scope.areaExtensiva =
        numeroPisos: 1
        numeroViviendas: validacion.faltante
        superficieMinimaPredio: 0
        superificieMinimaVivienda: 0
        superificeTotalAreaExtensiva: 0

    # Se ejecuta cuando se preciona el boton cancelar
    $scope.cancelar = () ->
      $window.location.reload()

  # Agregar un Area extensiva mas a la marcacion
    $scope.agregarAreaExtensiva = () ->
      if $scope.areaExtensiva.superficieAreaUrbana? and $scope.areaExtensiva.superficieAreaUrbana > 0
        if $scope.areaExtensiva.numeroViviendas < 1
          toastr.warning 'La cantidad de Viviendas no puede ser ', 'AREA EXTENSIVA'
      else
        mostrarError()
        return

      $scope.areasExtensivas[$scope.posAreaExtensiva] = $scope.areaExtensiva

      validacion = $scope.controlarCantidadViviendas()
      $scope.validacion = validacion if not validacion.error

      # Verificando que no se haya exedido del numero de viviendas
      if validacion.error
        $scope.areasExtensivas[$scope.posAreaExtensiva] = undefined
        toastr.error "La cantidad de viviendas que declaro fue #{$filter('number')(validacion.maximo,2)} y usd esta ingresando #{$filter('number')(validacion.actual,2)}", "AREAS EXTENSIVAS"
        return

      # En caso que se completen los pasos pero no la cantidad de viviendas
      if validacion.cantidadValida and !validacion.estado
        $scope.areasExtensivas[$scope.posAreaExtensiva] = undefined
        $scope.validacion = $scope.controlarCantidadViviendas()
        toastr.warning "Debe definir todas la viviendas dentro de esta area extensiva", "AREAS EXTENSIVAS"
        return

      # Verificando que se hayan compleado el numero de viviendas
      if !validacion.estado
        toastr.warning "Le restan a incluir #{$filter('number')(validacion.faltante,2)} Viviendas", "AREAS EXTENSIVAS"
        $scope.posAreaExtensiva += 1
      else
        # Verificando que no se ingrese todas la viviendas sin antes completar las areas urbanas
        if validacion.cantidadValida
          WizardHandler.wizard().next()
        else
          $scope.areasExtensivas[$scope.posAreaExtensiva] = {}
          $scope.validacion = $scope.controlarCantidadViviendas()
          toastr.warning "La cantidad de viviendas debe distribuirse en las areas urbanas que definio", "AREAS EXTENSIVAS"
          return

      $scope.areaExtensiva =
        numeroPisos: 1
        numeroViviendas: validacion.faltante
        superficieMinimaPredio: 0
        superificieMinimaVivienda: 0
        superificeTotalAreaExtensiva: 0

    # Verifica el estado de las viviendas par no sobrepasar su limite
    $scope.controlarCantidadViviendas = () ->
      totalViviendas = 0
      cantidadAreasExtensivas = 0
      maxViviendas = $scope.calculo.areaExtensivaConstruirReq
      $scope.viviendasDisponibles = 0


      for areaExtensiva in $scope.areasExtensivas
        if areaExtensiva
          totalViviendas += areaExtensiva.numeroViviendas
          cantidadAreasExtensivas += 1
      estado =
        maximo: maxViviendas
        estado: (totalViviendas == maxViviendas)
        cantidadValida: $scope.calculo.cantidadAreasExtensivas == $scope.areasExtensivas.length
        faltante: (maxViviendas - totalViviendas)
        actual: totalViviendas
        error: (totalViviendas > maxViviendas)
      estado

    # Calcula los promedios
    $scope.calcularPromedios = () ->
      $scope.calculo.numeroViviendas = 0
      $scope.calculo.densificacionAreaExtensiva = 0
      $scope.calculo.porcentajeCoeficienteOcupacionUsoPrivado = 0
      $scope.calculo.coeficienteOcupacionUsoPrivado = 0
      $scope.calculo.porcentajeCoeficienteOcupacionUsoPublico = 0
      $scope.calculo.coeficienteOcupacionUsoPublico = 0

      cantidadAreasExtensivas = $scope.calculo.areasExtensivas.length
      promedio_pisos = 0

      for areaExtensiva in $scope.calculo.areasExtensivas
        promedio_pisos += areaExtensiva.numeroPisos / cantidadAreasExtensivas
        $scope.calculo.numeroViviendas += areaExtensiva.numeroViviendas

        $scope.calculo.coeficienteOcupacionUsoPrivado += (areaExtensiva.coeficienteOcupacionUsoPrivado)
        $scope.calculo.coeficienteOcupacionUsoPublico += (areaExtensiva.coeficienteOcupacionUsoPublico)

      $scope.calculo.superficieAreaUrbana = $scope.calculo.coeficienteOcupacionUsoPrivado + $scope.calculo.coeficienteOcupacionUsoPublico

      $scope.calculo.densificacionAreaExtensiva += ConstanteService.getCoheficiente(promedio_pisos)
      $scope.calculo.porcentajeCoeficienteOcupacionUsoPublico = $scope.calculo.coeficienteOcupacionUsoPublico / $scope.calculo.superficieAreaUrbana
      $scope.calculo.porcentajeCoeficienteOcupacionUsoPrivado = 1 - $scope.calculo.porcentajeCoeficienteOcupacionUsoPublico
      return
    return
