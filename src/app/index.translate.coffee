angular.module 'vivienda'
  .config ($translateProvider) ->
    'ngInject'

    $translateProvider.translations 'es',
      TITLE: 'MODELO BÁSICO PARA LA DELIMITACIÓN DE ÁRAS URBANAS'

      FIRST_TITLE: 'INFORMACIÓN GENERAL DEL ÁREA URBANA'

      DEPARTAMENTO: 'Departamento'
      DEPARTAMENTOPH: 'Ingrese el Departamento'
      MUNICIPIO: 'Municipio'
      MUNICIPIOPH: 'Ingrese el Municipio'
      AREA_URBANA: 'Nombre del Area Urbana'
      AREA_URBANAPH: 'Ingrese el Area Urbana'
      PROFESIONAL_SOLICITANTE: 'Nombre Solicitante'
      PROFESIONAL_SOLICITANTEPH: 'Ingrese su nombre'

      STEP_ONE_TITLE: 'DISTRIBUCIÓN DEL DÉFICIT HABITACIONAL CUANTITATIVO URBANO'
      POBLACION_AREA_EXTENSIVA: 'Cuál es la población actual del área urbana'
      POBLACION_AREA_EXTENSIVA_PH: 'Ingrese la población'
      CUAL_POBLACION_AREA_EXTENSIVA: 'Cuánto será el crecimiento poblacional proyectado del área urbana por un periodo de tiempo superior a los 5 años  '
      MIEMBROS_FAMILIA: 'Cuál es tamaño medio de hogar (Nº promedio de miembros de la familia) del área urbana'
      MIEMBROS_FAMILIA_PH: 'Ingrese la cantidad de Miembros'
      CINCO_ANIOS_REQUERIRA: 'Entonces, el déficit habitacional cuantitativo proyectado del área urbana es:'
      VIVIENDAS_CONSTRUIR: 'Cuántas viviendas del déficit habitacional cuantitativo proyectado se dispondrán en el área urbana intensiva'
      VIVIENDAS_CONSTRUIR_PH: 'Ingrese la canidad de Viviendas'
      AREA_CONSTRUIRAN: 'Entonces, en el área urbana extensiva se construirán:'
      CANTIDAD_AREAS_EXTENSIVAS: 'Ingrese la cantidad de Areas Extensivas'

      STEP_TWO_TITLE: 'CANTIDAD DE VIVIENDAS DEL DÉFICIT HABITACIONAL CUANTITATIVO POR DESIGNAR EN EL ÁREA URBANA EXTENSIVA:'
      CUANTAS_VIVIENDAS_CONSTRUIRAN: 'Cuantas viviendas se contruiran'
      CUANTAS_VIVIENDAS_CONSTRUIRAN_PH: 'Ingrese la cantidad de Viviendas'
      SUPERFICIE_MINIMA_PREVIO: 'Cuál es la superficie media del predio'
      SUPERFICIE_MINIMA_PREVIO_PH: 'Ingrese la Superfie Minima'
      SUPERFICIE_MINIMA_VIVIENDA: 'Cuál es la superficie estándar de la vivienda '
      PREDIO_SUPERFICIE_PATIO: 'Entonces, el lote de terreno tiene una superficie de patio o área libre de'
      AREA_EXTENSIVA: 'En el área extensiva se requerirá una superfice total de'
      NUMERO_DE_PISOS: '  De cuántos niveles o pisos promedio planifica tener las edificaciones en el área urbana extensiva'
      DENSIFICACION_AREA_EXTENSIVA: 'Entonces, el área extensiva tendra un indice de densificación de'
      SUPERFICIE_TOTAL_USO_SUELO_URBANO: 'La superficie Predial Neta – SPN para uso de suelo residencial será'
      SUPERFICIE_TOTAL_AREA_EXTENSIVA: 'Se requieren suelo para otros usos urbanos en una superficie total de'
      SUPERFICIE_TOTAL_AREA_EXTENSIVA_PH: 'Ingrese la superficie'
      COHEFICIENTE_OCUPACION_USO_PRIVADO: 'Entonces, el coeficiente porcentual de ocupación del suelo para usos privados será:'
      COHEFICIENTE_OCUPACION_USO_PUBLICO: 'Cuál es el coeficiente porcentual de ocupación de suelo urbano (o de cesión) para usos públicos (red vial, equipamiento, áreas verdes, etc.)'
      SUPERFICIE_TOTAL_AREA_EXTENSIVA_TOTAL: 'Entonces, la superficie total del Área Urbana Extensiva será  '

      STEP_THREE_TITLE: 'RESUMEN DE DATOS DE LA DAU'
      TOTAL_VIVIENDAS_AREA_EXTENSIVA: 'Cuantas viviendas se contruiran'
      SUPERFICIE_AREA_URBANA_PROTECCTION_PH: 'Ingrese la superficie'
      SUPERFICIE_AREA_URBANA_PROTECCTION: 'La superficie total del área urbana de proteccion'
      SUPERFICIE_AREA_URBANA_AGREOPECUARIA: 'La superficie total del área urbana de produccion agro pecuaria'
      SUPERFICIE_FUTURA_AREA_URBANA: 'SUPERFICIE DE LA FUTURA AREA URBANA'

      STEP_FOUR_TITLE: 'VIVIENDA SOCIAL'
      VIVIENDAS_SOCIALES_AREA_INTENSIVA: '¿Cuántas viviendas sociales quiere construir en el área urbana intensiva?'
      VIVIENDAS_SOCIALES_AREA_EXTENSIVA: '¿Cuántas viviendas sociales quiere construir en el área urbana extensiva?'
      TOTAL_VIVIENDAS_SOCIALES: 'TOTAL DE VIVIENDAS SOCIALES EN EL ÁREA URBANA'

      NEXT: 'SIGUIENTE'
      NUEVO: 'AGREGAR NUEVA AREA EXTENSIVA'
      FINISH: 'FINALIZAR'

      VALIDAR_CAMPOS: 'DEBE LLENAR TODOS LOS CAMPOS'

    $translateProvider.preferredLanguage 'es'
    return
