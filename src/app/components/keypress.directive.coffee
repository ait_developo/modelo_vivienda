angular.module 'vivienda'
  .directive 'keyEnter', () ->
    {
      restrict: 'A'
      link: (scope, element, attrs, ctrl) ->
        $(element).find('input').keypress((e) ->
          if e.which == 13
            self = @
            pos = 0
            inputs = $(element).find("input[tabIndex!=-1]")
            for input in inputs
              pos += 1
              if $(input)[0].id == $(self)[0].id and pos < inputs.length
                # if $(inputs)[pos].tabIndex == -1
                #   if (pos + 1) < inputs.length
                #     $(inputs)[pos + 1].focus()
                # else
                $(inputs)[pos].focus()
            e.preventDefault()
        )
    }
