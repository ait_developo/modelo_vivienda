angular.module 'vivienda'
  .service 'RegionService', () ->
    # Retorna todos los departametnos
    @getDepartamentos = () ->
      departamentos = [
        {name: 'La Paz'}
        {name: 'Chuquisaca'}
        {name: 'Santa Cruz'}
        {name: 'Oruro'}
        {name: 'Beni'}
        {name: 'Pando'}
        {name: 'Cochabamba'}
        {name: 'Potosi'}
        {name: 'Tarija'}
      ]
      departamentos
    return