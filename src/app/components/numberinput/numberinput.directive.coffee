angular.module 'vivienda'
  .directive 'numberInput', ($filter, $locale) ->
    'ngInject'
    getCaretPosition = (inputField) ->
      # Initialize
      position = 0
      # IE Support
      if document.selection
        inputField.focus()
        # To get cursor position, get empty selection range
        emptySelection = document.selection.createRange()
        # Move selection start to 0 position
        emptySelection.moveStart 'character', -inputField.value.length
        # The caret position is selection length
        position = emptySelection.text.length
      else if inputField.selectionStart or inputField.selectionStart == 0
        position = inputField.selectionStart
      position

    setCaretPosition = (inputElement, position) ->
      if inputElement.createTextRange
        range = inputElement.createTextRange()
        range.move 'character', position
        range.select()
      else
        if inputElement.selectionStart
          inputElement.focus()
          inputElement.setSelectionRange position, position
        else
          inputElement.focus()
      return

    countNonNumericChars = (value) ->
      (value.match(/[^a-z0-9]/gi) or []).length

    {
      require: 'ngModel'
      restrict: 'A'
      link: ($scope, element, attrs, ctrl) ->
        fractionSize = parseInt(attrs['fractionSize']) or 0
        numberFilter = $filter('number')
        #format the view value
        
        ctrl.$formatters.push (modelValue) ->
          retVal = numberFilter(modelValue, fractionSize)
          isValid = isNaN(modelValue) == false
          ctrl.$setValidity attrs.name, isValid
          retVal
        
        ctrl.$parsers.push (viewValue) ->
          caretPosition = getCaretPosition(element[0])
          nonNumericCount = countNonNumericChars(viewValue)
          viewValue = viewValue or ''
          trimmedValue = viewValue.trim().replace(/,/g, '').replace(/`/g, '').replace(/'/g, '').replace(/\u00a0/g, '').replace(RegExp(' ', 'g'), '')
          separator = $locale.NUMBER_FORMATS.DECIMAL_SEP
          arr = trimmedValue.split(separator)
          decimalPlaces = arr[1]
          console.log decimalPlaces
          if decimalPlaces?
            if decimalPlaces.length > fractionSize
              #Trim extra decimal places
              decimalPlaces = decimalPlaces.substring(0, fractionSize)
              trimmedValue = arr[0] + separator + decimalPlaces
          numericValue = parseFloat(trimmedValue)
          isEmpty = numericValue == null or viewValue.trim() == ''
          isRequired = attrs.required or false
          isValid = true
          if isEmpty and isRequired
            isValid = false
          if isEmpty == false and isNaN(numericValue)
            isValid = false
          ctrl.$setValidity attrs.name, isValid
          if isNaN(numericValue) == false and isValid
            newViewValue = numberFilter(numericValue, fractionSize)
            element.val newViewValue
            newNonNumbericCount = countNonNumericChars(newViewValue)
            diff = newNonNumbericCount - nonNumericCount
            newCaretPosition = caretPosition + diff
            if nonNumericCount == 0 and newCaretPosition > 0
              newCaretPosition--
            setCaretPosition element[0], newCaretPosition
          if isNaN(numericValue) == false then numericValue else null
        return
    }

