angular.module 'vivienda'
  .service 'ReportService', () ->
    'ngInject'
    
    @getDataUri = (url, callback) ->
      image = new Image

      image.onload = ->
        canvas = document.createElement('canvas')
        canvas.width = @naturalWidth
        # or 'width' if you want a special/scaled size
        canvas.height = @naturalHeight
        # or 'height' if you want a special/scaled size
        canvas.getContext('2d').drawImage this, 0, 0
        # Get raw image data
        callback canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, '')
        # ... or get as Data URI
        callback canvas.toDataURL('image/png')
        return

      image.src = url
      return
    return