angular.module 'vivienda'
  .service 'ConstanteService', () ->
    'ngInject'
    
    N = 100
    @BASE = 10000

    @getCoheficiente = (numeroPisos) ->
      (N / numeroPisos) / 100

    @redondear = (num, digitos=2) ->
      aux = 10 ** digitos
      Math.round(num * aux) / aux
    return